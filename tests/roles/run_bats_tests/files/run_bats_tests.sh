#!/bin/bash
#
# Run bats tests for a given $TEST_PACKAGE, e.g. buildah, podman
#
# This is invoked by the 'run_bats_tests' role; we assume that
# the package foo has a foo-tests subpackage which provides the
# directory /usr/share/foo/test/system, containing one or more .bats
# test files.
#

export PATH=/usr/local/bin:/usr/sbin:/usr/bin

# Keep all logs in /tmp/artifacts - this seems to be an undocumented
# (and therefore dangerous and unreliable) convention of the Standard
# Test Roles package. As of 2020-05 we have to coexist with cockpit
# which uses standard-test-basic, which means we need to conform to
# its conventions.
# We rely on our parent playbook to create /tmp/artifacts and make it
# world-writable so nonroot tests can use it.
TEST_LOG_TXT=/tmp/artifacts/test.log
TEST_LOG_YML=/tmp/artifacts/results.yml

# "podman root" -> "podman-root"
testname_oneword=${TEST_NAME// /-}

FULL_LOG=/tmp/artifacts/test.${testname_oneword}.debug.log
BATS_LOG=/tmp/artifacts/test.${testname_oneword}.bats.log
rm -f $FULL_LOG $BATS_LOG
touch $FULL_LOG $BATS_LOG

exec &> $FULL_LOG

# Log program versions
echo "Packages:"
rpm -qa |\
    egrep 'buildah|conmon|container|crun|iptable|podman|runc|skopeo|slirp|systemd' |\
    sort |\
    sed -e 's/^/  /'

echo "------------------------------"
printenv | sort

testdir=/usr/share/${TEST_PACKAGE}/test/system

if ! cd $testdir; then
    echo "FAIL ${TEST_NAME} : cd $testdir"      >> $TEST_LOG_TXT
    echo "- { test: '${TEST_NAME}', result: error, logs: [ $(basename $FULL_LOG) ] }" >> $TEST_LOG_YML
    exit 0
fi

if [ -e /tmp/helper.sh ]; then
    echo "------------------------------"
    echo ". /tmp/helper.sh"
    . /tmp/helper.sh
fi

if [ "$(type -t setup)" = "function" ]; then
    echo "------------------------------"
    echo "\$ setup"
    setup
    if [ $? -ne 0 ]; then
        echo "FAIL ${TEST_NAME} : setup"       >> $TEST_LOG_TXT
        echo "- { test: '${TEST_NAME}', result: error, logs: [ $(basename $FULL_LOG) ] }" >> $TEST_LOG_YML
        exit 0
    fi
fi

echo "------------------------------"
echo "\$ bats ."
bats . &> $BATS_LOG
rc=$?

echo "------------------------------"
echo "bats completed with status $rc"

status=PASS
if [ $rc -ne 0 ]; then
    status=FAIL
fi

echo "${status} ${TEST_NAME}" >> $TEST_LOG_TXT

# Append a stanza to results.yml
(
    echo "- test: ${TEST_NAME}"
    # pass/fail - the ',,' (comma comma) converts to lower-case
    echo "  result: ${status,,}"
    echo "  logs:"
    echo "  - $(basename $BATS_LOG)"
    echo "  - $(basename $FULL_LOG)"
)  >> $TEST_LOG_YML


if [ "$(type -t teardown)" = "function" ]; then
    echo "------------------------------"
    echo "\$ teardown"
    teardown
fi

# FIXME: for CI purposes, always exit 0. This allows subsequent tests.
exit 0
